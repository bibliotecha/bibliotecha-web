#! /usr/bin/env python
# -*- coding: utf-8 -*-

#################
#  Upon receiving an uploaded books - via festivals online upload - and ge
#  SCRIPT: adds the file-name and metadata provided by users to xml dabatase
##################
from lxml import etree
import sys, os, datetime

name = unicode(sys.argv[1], 'utf-8')
book_title = unicode(sys.argv[2], 'utf-8')
book_author = unicode(sys.argv[3], 'utf-8')
book_file = unicode(sys.argv[4], 'utf-8')
catalog_file = 'catalog_{0}.xml'.format(name)
catalog_path = '/home/andre/Documents/Projects/Bibliotecha/code/'


def edit_catalog(this_tree, this_root, this_title, this_author, this_file):
    now = (datetime.datetime.now()).isoformat()
    newbook=etree.SubElement(this_root, "book")
    newbook.set('date', now)
    etree.SubElement(newbook, 'title').text = this_title
    etree.SubElement(newbook, 'author').text = this_author
    etree.SubElement(newbook, 'file').text = this_file
    this_tree.write('catalog_{0}.xml'.format(name), pretty_print=True, encoding='utf-8')

def create_catalog(catalog_name):
    root = etree.Element('catalog_{0}'.format(catalog_name)) # create xml
    tree = etree.ElementTree(root) 
    tree.write('catalog_{0}.xml'.format(catalog_name), pretty_print=True) # save to file
    edit_catalog(tree, root, book_title, book_author, book_file)

def open_catalog(catalog_name):
    parser = etree.XMLParser(remove_blank_text=True) #to ensure pretty_print of edited file
    tree = etree.parse("catalog_{0}.xml".format(catalog_name), parser)
    root = tree.getroot()
    edit_catalog(tree, root, book_title, book_author, book_file)



if not os.path.exists(catalog_path + catalog_file):
    print "file is MISSING"
    create_catalog(name)
else:
    print "open catalog"
    open_catalog(name)



# XML READER - Calibre add
