#! /usr/bin/env python
# -*- coding: utf-8 -*-
########
# SCRIPT: adds submitted books (only) to calibre library, according to the catalog chooser
# REQUIRED ARGS: catalog_path books_path calibre_di4
######

from lxml import etree
import sys, subprocess

print sys.argv
if len(sys.argv) < 4:
     print "ERROR: arguments are  (CATALOG PATH) is missing.", "NECESSARY args catalog_path books_path calibre_dir"

catalog_path = sys.argv[1]
#catalog_file = 'catalog_{0}.xml'.format(catalog_name)
#catalog_path = '/home/andre/Documents/Projects/Bibliotecha/code/'
books_path = sys.argv[2] #'/home/andre/'
calibre_dir = sys.argv[3] # "~/CalibreLibrary"

tree = etree.parse(catalog_path)
books = tree.xpath('//book')

for book in books:
     title = (book.xpath('./title'))[0].text
     author = (book.xpath('./author'))[0].text
     bookfile = (book.xpath('./file'))[0].text
     print type(title), title, type(author), author
     s=unicode("This is {0}, by {1}, in file {2}").format( title, author, bookfile)
     print s
     bookfile = "matrix_printer_lx300pul.pdf" 
     subprocess.call(["calibredb", 
"add",  
books_path + bookfile, 
unicode("--library-path={0}").format("~/CalibreLibrary"), 
unicode("--title={0}").format(title), 
unicode("--authors={0}").format(author), 
books_path + bookfile])


