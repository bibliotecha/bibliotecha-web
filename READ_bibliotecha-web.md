# Directories #

* cgi-bin

* lib  - for css, libraries and plugins

* upload for online book uploads - creation of initial catalogs for events
    * each event has is own directory. 
	* the html page is the same with only the event name changed.




# File Tree #
```
bibliotecha-web
    |
    ├── cgi-bin (executables. Right now living in pzi3 server)
	│   ├── catalog_entry.py 
	│   └── receive_book.cgi
	├── lib (libraries, style sheets, plugins)
	│   ├── jquery-1.11.0.min.js
	│   └── style_other_pages.css
	├── READ_bibliotecha-web
	└── upload (static html pages)
	        ├── amro
		│      ├── upload_book.html
		│	
		└── offthepress
		        ├── upload_book.html
```
