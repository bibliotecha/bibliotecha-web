#! /usr/bin/env python
# -*- coding: utf-8 -*-

# SCRIPT: adds uploaded books metadata to a catalog 

from lxml import etree
import sys, os, datetime, subprocess

upload_path = sys.argv[1]
event = (upload_path.split('/'))[-2] #will give name to the dir and catalog
catalog_path = '/home/acastro/public_html/cgi-bin/books/{0}/{0}.xml'.format(event) # NEEDS CHG IN SERVER

#catalog_file = 'catalog_{0}.xml'.format(catalog)

book_title = unicode(sys.argv[2], 'utf-8')
book_author = unicode(sys.argv[3], 'utf-8')
book_file = unicode(sys.argv[4], 'utf-8')

print upload_path
print event
print catalog_path
def edit_catalog(this_tree, this_root, this_title, this_author, this_file):
    now = (datetime.datetime.now()).isoformat()
    newbook=etree.SubElement(this_root, "book")
    newbook.set('date', now)
    etree.SubElement(newbook, 'title').text = this_title
    etree.SubElement(newbook, 'author').text = this_author
    etree.SubElement(newbook, 'file').text = this_file
    this_tree.write(catalog_path, pretty_print=True, encoding='utf-8')

def create_catalog(catalog_name):
    root = etree.Element(catalog_name) 
    tree = etree.ElementTree(root) 
    tree.write(catalog_path)
    edit_catalog(tree, root, book_title, book_author, book_file)

def open_catalog(catalog_name):
    parser = etree.XMLParser(remove_blank_text=True) #to ensure pretty_print of edited file
    tree = etree.parse(catalog_name, parser)
    root = tree.getroot()
    edit_catalog(tree, root, book_title, book_author, book_file)



if not os.path.exists(catalog_path):
    create_catalog(event)
else:
    open_catalog(catalog_path)



#test


