#!/usr/bin/env python
import cgi 
import cgitb; cgitb.enable()

import os, subprocess, sys

# SCRIPT: stores files, sends user notification, runs subprocess to include filename and metadata into catalog
# NOTE: It runs on pzi3 server!!


print "Content-type: text/html"
print

html="""
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="http://bibliotecha.info/lib/style_other_pages.css" type="text/css" media="screen" />
    <link href="http://bibliotecha.info/favicon.ico" rel="icon" type="image/x-icon" />
    <title>Bibliotecha | Upload</title>
 </head>
  <body>
<h3>
{0}
</h3>
<p><a href='{1}'>back to the upload page</a></p>
<pre id="bibliotechaBadge">      
      + + + + + + + + + + +
      +                   +
      +  @             *  +
      +                   +
      +    Bibliotecha    +
      +   Softwarestack   +
      +                   +
      +     (       )     +
      +  #             $  +
      +         %         +
      + + + + + + + + + + + 
    <a href='http://bibliotecha.info/'>bibliotecha.info</a>

    </pre>
</a>
<br/><br/>
</body>
</html>
"""
url = os.environ.get("HTTP_REFERER")
event =  (url).split("/")[-2] # What event is the url #URL MUST BE http://../../upload_book/ofthepress.html


form = cgi.FieldStorage()
title = form.getvalue("title",  "")
author1 = form.getvalue("author1",  "") # CHANGE TO AUTHOR1,2, 3
author2 = form.getvalue("author2",  "")
author3 = form.getvalue("author3",  "")

authors = author1 # authors for calibre. Structure: 1stName Surname, 1stName Surname, 1stName Surname
if author2 != "":
   authors = authors + ", " + author2
if author3 != "":
   authors = authors + ", " + author3

book =  form["book"]# form.getvalue("book", "")
filename = book.filename
upload_path = "/home/acastro/public_html/cgi-bin/books/{0}/".format(event) # NEEDS BE CHANGED IN SERVER

# save the book
if book.filename: # # Test if the file was uploaded   
   fn = os.path.basename(book.filename)#return the final component of a pathname
   fn = fn.replace(" ", "")
   ext = os.path.splitext(fn)[1]#return the extension

   if ext in [".pdf", ".epub", ".mobi", ".txt", ".djvu"]:
      bookfile =open(upload_path + fn, 'wb')
      bookfile.write(book.file.read())

      with open(os.devnull, 'w') as fp: # add metadata and filename to respective index
         cmd = subprocess.Popen(['python', '/home/acastro/public_html/cgi-bin/catalog_entry.py', "{0}".format(upload_path), "{0}".format(title), "{0}".format(authors), filename], stdout=fp)
      reply = "<p>Thank you for your contribution</p><p>{0} by {1} was added to <a href='http://bibliotecha.info'>Bibliotecha's</a> catalog</p>".format(fn, authors)
      print html.format(reply, url)

   else:
      print html.format("<p>It seems like the file what you are trying to upload is not a book.<br/> Bibliotecha only accepts pdf, epub, mobi, txt, djvu file formats. Please try again.</p>", url)

else:
   print html.format("<p>It was not possible to add your submission to Bibliotecha's catalog</p><p>Please try again or contact the Bibliotecha librarians at mail@bibliotecha.info </p>", url)



